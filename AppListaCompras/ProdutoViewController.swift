//
//  ProdutoViewController.swift
//  AppListaCompras
//
//  Created by iossenac on 14/11/18.
//  Copyright © 2018 iossenac. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ProdutoViewController: UIViewController {
    @IBOutlet weak var txtNome: UITextField!
    
    @IBOutlet weak var txtQuantidade: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Adicionar(_ sender: Any) {
        let db = Database.database().reference()
        let produtos = db.child("produtos")
        let dados = ["nome":txtNome.text,"quantidade":txtQuantidade.text]
        produtos.childByAutoId().setValue(dados)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
