//
//  CadastroViewController.swift
//  AppListaCompras
//
//  Created by iossenac on 14/11/18.
//  Copyright © 2018 iossenac. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class CadastroViewController: UIViewController {

    @IBOutlet weak var txtNome: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtSenha: UITextField!
    @IBOutlet weak var txtConfirmaSenha: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func salvar(_ sender: Any) {
        if let email = txtEmail.text {
            if let senha = txtSenha.text {
                if let confirmaSenha = txtConfirmaSenha.text {
                    if senha == confirmaSenha {
                        let autenticacao = Auth.auth()
                        autenticacao.createUser(withEmail: email, password: senha) { (usuario, erro) in
                            if erro == nil {
                                if usuario == nil {
                                    self.erro("Problema ao criar usuario")
                                }else{
                                    let banco = Database.database().reference()
                                    let usuarios = banco.child("usuarios")
                                    let dados = ["nome": self.txtNome.text!, "email": email]
                                    usuarios.child(usuario!.user.uid).setValue(dados)
                                    print("sucesso")
                                    self.performSegue(withIdentifier: "segueListaProdutos", sender: nil )
                                }
                            } else {
                                self.erro("Problema ao realizar autenticação")
                            }
                        }
                        //autenticacao.createUser(withEmail: email, password: senha, completion: )
                    } else {
                        erro( "Senhas não conferem")
                    }
                }
            }
        }
        
    }
    
    func erro(_ mensagem : String) {
        exibirMensagem(titulo: "Erro", mensagem: mensagem)
    }
    
    func exibirMensagem(titulo : String, mensagem : String) {
        let alerta = UIAlertController(title: titulo, message: mensagem, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default)
        alerta.addAction(ok)
        present(alerta, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
